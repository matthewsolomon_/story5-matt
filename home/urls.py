
from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('halaman2/', views.halaman2, name='halaman2'),
]