# Generated by Django 2.2.5 on 2020-10-16 06:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkuliah',
            name='dosen',
            field=models.TextField(max_length=60),
        ),
        migrations.AlterField(
            model_name='matkuliah',
            name='ruangan',
            field=models.CharField(max_length=60),
        ),
    ]
