from django.db import models
from django.shortcuts import reverse
from django.utils import timezone

semesteran = [
    ("2019/2020 Gasal","2019/2020 Gasal"),
    ("2019/2020 Genap","2019/2020 Genap"),
    ("2020/2021 Gasal","2020/2021 Gasal"),
    ("2020/2021 Genap","2020/2021 Genap"),
    ("2021/2022 Gasal","2021/2022 Gasal"),
    ("2021/2022 Genap","2021/2022 Genap"),
    ("2022/2023 Gasal","2022/2023 Gasal"),
    ("2022/2023 Genap","2022/2023 Genap")
]

class matkuliah(models.Model):
    nama = models.TextField(max_length = 60)
    dosen = models.TextField(max_length = 60)
    sks = models.IntegerField()
    desc = models.TextField(max_length = 800)
    sems = models.TextField(choices = semesteran)
    ruangan = models.CharField(max_length = 60)

    def __str__(self):
        return "{} ({})".format(self.nama, self.dosen)

    def get_absolute_url(self):
        return reverse("story5:seematkul",args=[self.id])
