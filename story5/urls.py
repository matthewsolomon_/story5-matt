
from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('addmatkul/', views.addmatkul, name='addmatkul'),
    path('deletematkul/<int:id>', views.deletematkul, name='deletematkul'),
    path('seematkul/<int:id>', views.seematkul, name='seematkul'),
]