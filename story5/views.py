from django.shortcuts import render, redirect
from django.contrib import messages
from .models import matkuliah
from .forms import formatkul

def index(request):
    diction = {
        'page' : 'index',
        'listmatkul' : matkuliah.objects.all().order_by("id")
    }
    return render(request, 'story5/index.html', diction)

def addmatkul(request):
    if request.method == 'POST':
        form_matkul = formatkul(request.POST)
        if form_matkul.is_valid():
            form_matkul.save()
            messages.add_message(request, messages.SUCCESS, 'Mata Kuliah {} telah ditambahkan'.format(request.POST['nama']))
        return redirect('story5:index')
    diction = {
        'page' : 'addmatkul'
    }
    return render(request, 'story5/addmatkul.html', diction)

def deletematkul(request, id):
    if request.method == 'POST':
        matakuliah = matkuliah.objects.get(id=id)
        matakuliah.delete()
        messages.add_message(request, messages.SUCCESS, 'Menghapus mata kuliah {} dari daftar'.format(matakuliah.nama))
    return redirect('story5:index')


def seematkul(request, id):
    diction = {
        'matakuliah' : matkuliah.objects.get(id=id)
    }

    return render(request, 'story5/seematkul.html', diction)

